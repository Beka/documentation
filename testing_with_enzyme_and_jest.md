# Testing ReactJS components with Enzyme and Jest

## Installation and documentation
- Enzyme --> http://airbnb.io/enzyme/
- Jest --> https://facebook.github.io/jest/

### React adapters
You will have to install a specific adapter based on the ReactJS version you are using. Here is a step by step guide how to do it:

- In package.json:
```json
"jest":
  "setupFiles": [
    "location_of_your_jest_setup_file"
  ]
```
- In your setup file:
```javascript
import { configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-<react-version>';

configure({ adapter: new Adapter() });
```

<hr />

## Using Enzyme shallow render
```javascript
import {shallow} from 'enzyme';

it('should render a shallow component', () => {
  const comp = shallow('<App />', {context: {}, disableLifecycleMethods: bool});
});
```
It will render a shallow component, which means all other components which are composed in this one will not be rendered but they will be shown as other components.

EXAMPLE of the `comp.debug();` output:
```html
<div>
  <h1>Hello World</h1>
  <OtherComponent title="foo" />
</div>
```

### Finding nodes in shallow rendered component
Based on the output example above, here is a snippet how it looks like to get the `h1` node and assert it:
```javascript
expect(comp.find('h1').length).toBe(1);
```
- You can use find with any css selector (class, id, element...)
- You can also assert the content: `.find('h1').text()).toBe('Hello World')`
- Find content by alt tag: `.find({alt: 'foo'})`
- Find content by props: `.find('[title="foo"]'`

### Testing using Jest snapshots
Based on the above test case example we can assert if the component is rendered correctly with a snapshot:
```javascript
expect(comp.toJSON()).toMatchSnapshot();
```
- This will create a new snapshot file
- You can check the snapshot for the correctly rendered structure of your component
- To be easy debuggable we use `toJSON()`

### Testing props of the component
There are two ways which are the most common ways of testing component's props. One is simply using Jest snapshot and the other one is asserting each passed prop.

#### Using Jest snapshot:
```javascript
expect(comp.toJSON()).toMatchSnapshot();
```
Simply create a snapshot of your component and check it manualy if the props you passed to the component are correctly rendered.

#### Using assertion:
```javascript
const comp = shallow(<Component title="foo" />);
expect(comp.instance().props.title).toBe('foo');
```

All component's props are accessible in it's instance. We can also assert if the HTML element is rendered correctly if a certain props are passed:
```javascript
const comp = shallow(<Link url="bar" />);
expect(comp.props().href).toBe('bar');
```

We can access the rendered component's HTML attributes by calling `props()` on it.

We can also access all component's methods:
```javascript
const comp = shallow(<App />);
const methodResult = comp.instance().myMethod();
```

As we can see here, we can fire any method from our mounted component and we can easily assert its results.

<hr />

## Using Enzyme mount render
Mount is usefull when we are trying to test React lifecycles events. When using the mount render from Enzyme we must use an environment with the same API as a browser. (for example JSDom library).

Example of a render mount:
```javascript
const comp = mount(<App />, {context: {}, attachTo: DOMElement});
comp.unmount();
```

As a second parameter of the mount method we can optionaly pass an object containing the context data and the attachTo DOM element, if we want to attach this component to a specific DOM location.

We can also use the unmount method, which will unmount the component from the DOM.

<hr />

## Simulate events
```javascript
const comp = shallow(<App />);
const button = comp.find('button');

expect(comp.find('.status-text').text()).toBe('No!');
button.simulate('click');
expect(comp.find('.status-text').text()).toBe('Yes!');
```

Let's say that the App component has an event to change the text on en element with a class `.status-test`. By default the text is `No!` but once the user click on a button the text changes to `Yes!`. The test above will assert this use case by simulating a click event.

<hr />

## Testing component's state changes
```javascript
const comp = shallow(<App />);
comp.setState({foo: 'bar'});

expect(comp.toJSON()).toMatchSnapshot();
```

We can use the setState method on the tested component. We can easily verify the DOM changes by checking if the snapshot was changed.

<hr />

## Testing React lifecycle methods
```javascript
jest.spyOn(App.prototype, 'componentDidMount');
const comp = shallow(<App />);

expect(App.prototype.componentDidMount.mock.calls.length).toBe(1);
```

We can spy on component prototype chain for one of the lifecycle methods. To verify if the method was called we can assert its calls lenght. If there is no componentDidMount method set on this component the test will fail.

<hr />

## Testing Redux connected components
The key point is to import a non connected component. The connected components are normally exported as defaults. We should always export also the non connected component so it can be used in the unit tests: <br />
`import {Component} from 'Component';`

Example:
```javascript
it('calls addTodo Redux action creator with button click', () => {
  const props = {
    addTodo: jest.fn(),
    todos: []
  }
  const comp = shallow(<TodoList {...props} />);
  comp.find('input').simulate('change', {currentTarget: {value: 'Foo'}});
  comp.find('button').simulate('click');

  expect(props.addTodo).toHaveBeenCalledWith({text: 'Foo'});
  expect(comp.toJSON()).toMatchSnapshot();
});
```

The following happens here:
- We are mocking the Redux addTodo action creator
- We fire a change event on the input field, populating it with 'Foo'
- We fire a click event, which will call addTodo passing the data from the input field
- We assert that the addTodo method was called with the passed data 'Foo'
- At the end is always good to make a snapshop of the whole component to verify what it renders

Because we are using a non connected component, we can mock the Redux state and its methods. Then we can use it to assert the logic of the component itself.

<hr />

## Example: Testing React forms
Lets imagine that we have a form with some inputs and we are doing a simple submit of the form. On the submit we are calling an API endpoint `api.addUser`:
```jsx
<form data-testid='addUserForm' onSubmit={this.handleSubmit}>
  <input data-testid='name' type='text' onChange={this.handleChange('name')} placeholder='Name' value={this.state.name} />
  <button data-testid='submitButton' type='submit'>Submit</button>
</form>
```

What we need to test is the following:
- If users are able to input their information
- If users can submit the form, calls API
- Match snapshot

Lets do this:
### User is able to fill the form
```javascript
it('should be able to fill the form correctly', () => {
  const comp = shallow(<Form />);
  const input = comp.find('[data-testid="name"]');

  input.simulate('change', {currentTarget: {value: 'Foo'}});

  expect(input.props().value).toBe('Foo');
});
```
Here we are testing if the user was able to enter data in the input field:
- Using Enzyme shallow to render the component
- Finding the proper input field
- Simulating the onchange event on the input field and assigning some dummy value to it
- Asserting that the data in the input field has changed to the right value

### User is able to submit the form to a proper API
```javascript
it('should handle the submit of the form', () => {
  jest.spyOn(api, 'addUser').mockImplementation(() => Promise.resolve({data: 'Foo'}))
  
  const comp = shallow(<Form />)
  const input = comp.find('[data-testid="name"]');

  input.simulate('change', {currentTarget: {value: 'Foo'}});
  comp.find('[data-testid="addUserForm"]').simulate('submit', {preventDefault: () => {}})

  expect(api.addUser).toHaveBeenCalledWith('Foo');
});
```
Here we are testing if the user is able to submit the form with proper data:
- Create a shallow component
- Find the input field and simulate the onchange event to change the input field value
- Find the form element and simulate the submit action
- The submit action should call api.addUser
- Assert that the api.callUser was called with correct values